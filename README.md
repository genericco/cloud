# Welcome to the Generic Cloud repo

### Here you can find, track, and contribute to the underlying infrastructure of Generic Cloud Services.
It's a tad empty at the moment, but it will be filled soon! Feel free to take a look at the [issue board](https://gitlab.com/genericco/infrastructure/-/boards) to see current tasks and progress.
